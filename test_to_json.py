#!/usr/bin/env python3

import csv
import json
import re
import unittest

from Bio import SeqIO
from io import StringIO
from collections import Counter
from urllib.parse import urlparse

from os.path import dirname, realpath, join
script_dirname = dirname(realpath(__file__))

from to_json import to_json
from type_variants import parse_variants_in, lookup_variants, Variant, summarise_result

allowed_patterns = [
  r'^aa:[^:]+:[A-Z*\-]\d+[A-Z*\-]$',
  r'^snp:[ACGTN-]\d+[ACGTN-]$',
  r'^del:\d+:\d$'
]

def is_url(url):
  if not re.match(r'^\S+$', url):
    return False
  try:
    o = urlparse(url)
    return o.scheme in ['http', 'https', 'ftp', 'sftp']
  except:
    return False

class TestToJson(unittest.TestCase):
  def test_mutations(self):
    with open(join(script_dirname, "mutations.csv"), "r") as variants_in:
      csv_reader = csv.DictReader(variants_in)
      rows = [(r['mutation'], r['source']) for r in csv_reader]
    mutations = [m for m, *other in rows]

    errors = []
    
    mutation_counts = Counter(mutations)
    duplicates = [m for m,c in mutation_counts.items() if c > 1]
    if duplicates:
      errors.append(f"{','.join(duplicates)} were duplicated")
    
    bad_mutations = mutations
    for p in allowed_patterns:
      bad_mutations = [m for m in bad_mutations if not re.match(p, m)]
    if bad_mutations:
      errors.append(f"{','.join(bad_mutations)} are not in the right format")

    without_sources = [m for m, url in rows if not is_url(url)]
    if without_sources:
      errors.append(f"{','.join(without_sources)} need sources")
    
    self.assertEqual(len(errors), 0, "\n".join(["Errors in mutations.csv:"] + errors))
    
    with open(join(script_dirname, "mutations.csv"), "r") as variants_in:
      refseq = list(SeqIO.parse(join(script_dirname, "MN908947.fa"), "fasta"))[0].seq
      variants = parse_variants_in(variants_in, refseq)
  
  def test_e2e(self):
    with open(join(script_dirname, "mutations.csv"), "r") as variants_in:
      refseq = list(SeqIO.parse(join(script_dirname, "MN908947.fa"), "fasta"))[0].seq
      variants = parse_variants_in(variants_in, refseq)
    
    for prefix in ["MW430966", "England_EXET-13FD2E_2020", "England_MILK-D74DE5_2020"]:
      with open(join(script_dirname, f"test_data/{prefix}.aligned.fasta"), "r") as f:
        results = lookup_variants(f, variants)

      result, = results.values()
      r_file = StringIO()
      to_json(result, r_file)
      r_file.seek(0)
      actual_variants = { v['name']: v for v in json.load(r_file)['variants'] }

      with open(join(script_dirname, f"test_data/{prefix}.json"), "r") as f:
        expected_variants = { v['name']: v for v in json.load(f)['variants'] }
      
      for name, variant in expected_variants.items():
        self.assertEqual(variant, actual_variants[name], f"{prefix}: Expected {variant}, got {actual_variants[name]} for {name}")


class TestTyping(unittest.TestCase):
  def test_lookup(self):
    variants = [
      Variant("K1T", "aa:cds1:K1T", "aa", "K", "T", 10, 13, "fake"),
      Variant("A9C", "snp:A9C", "snp", "A", "C", 9, 10, "fake"),
      Variant("del:4:3", "del:4:3", "del", "AAA", "---", 4, 7, "fake"),
    ]
    seq1 = StringIO(">query\nAAAAAAAAAAAAAAA\n")
    result, = lookup_variants(seq1, variants).values()
    self.assertEqual([r for _,r,_ in result], ["K", "A", "AAA"])

    seq2 = StringIO(">query\nAAAA---AACACAAA\n")
    result, = lookup_variants(seq2, variants).values()
    self.assertEqual([r for _,r,_ in result], ["T", "C", "---"])

    seq3 = StringIO(">query\nAAAAAAAAAGACAAA\n")
    result, = lookup_variants(seq3, variants).values()
    self.assertEqual([r for _,r,_ in result], ["T", "G", "AAA"])

    summary = summarise_result(result)
    self.assertEqual(summary, dict(ref_count=1, alt_count=1, other_count=1, fraction_alt=0.3333))

    seq4 = StringIO(">query\nAAAA-N-AANANAAA\n")
    result, = lookup_variants(seq4, variants).values()
    self.assertEqual([s for _,_,s in result], ["sequence error", "sequence error", "sequence error"])

if __name__ == "__main__":
  unittest.main