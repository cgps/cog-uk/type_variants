#!/usr/bin/env python3

from Bio import SeqIO
from jinja2 import Environment, FileSystemLoader
from os.path import dirname, realpath, join
from collections import defaultdict

from type_variants import parse_variants_in

file_loader = FileSystemLoader('templates')
env = Environment(loader=file_loader)

def format_type(variant):
  return { "aa": "Amino Acid", "snp": "SNP", "del": "Deletion" }[variant.type]

def format_variant(variant):
  rows = []

  rows.append(dict(field="Type", value=format_type(variant)))
  if variant.type == "aa":
    rows.append(dict(field="CDS", value=variant.fullname.split(":")[1]))
  rows.append(dict(field="Position", value=variant.start))
  if variant.type == "del":
    rows.append(dict(field="Length", value=variant.end - variant.start))

  return dict(name=variant.name, rows=rows, source=variant.source)

if __name__ == '__main__':
  script_dirname = dirname(realpath(__file__))
  with open(join(script_dirname, "mutations.csv"), "r") as variants_in:
    refseq = list(SeqIO.parse(join(script_dirname, "MN908947.fa"), "fasta"))[0].seq
    variants = parse_variants_in(variants_in, refseq)
  variants = sorted(variants, key=lambda v: v.name)

  variant_groups = defaultdict(lambda: [])
  for variant in variants:
    variant_groups[format_type(variant)].append(format_variant(variant))

  template = env.get_template('mutations.html.j2')
  output = template.render(groups=variant_groups)

  with open(join(script_dirname, "public", "index.html"), "w") as index_file:
    index_file.write(output)
