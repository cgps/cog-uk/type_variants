# type_variants

A python script to type predefined variants in a fasta-format alignment of SARS-CoV-2 sequences, which must be aligned to Wuhan-Hu-1. See the [Genbank entry](https://www.ncbi.nlm.nih.gov/nuccore/MN908947.3) and the file `MN908947.fa` in this repository.

The scripts was created by [Ben Jackson](https://github.com/benjamincjackson).  CGPS have added to and ammended the script to output the data in JSON and to collect more information about known mutations.

We hope that users will contribute known mutations to [mutations.csv](mutations.csv).  These are meant to be a prioritised list of mutations of concern along with citations explaining why they may be relevant to the public health community.

## Usage


The variants to type must be defined by the user in [mutations.csv](mutations.csv).

This file has one row per variant of interest.  Rows must include the location of the mutation and a source URL.  In due course we hope to add information in the effects column (e.g. virulence, transmisability, vaccine escape).

Mutations must be one of `snp`, `del`, or `aa`.
* SNP mutations must start with `snp`, have the base in the reference, the 1-based offset into the reference, and the base in the mutation.  Bases should be uppercase. For example `snp:T6954C`
* Deletions must start with `del` followed by the 1-based start offset into the reference, and the number of bases which are expected to be deleted.  For example `del:11288:9`
* Amino acid changes should start with `aa` followed by the CDS it is found it, the amino acid in the reference, the 1-based offset of the allele in the CDS, and the amino acid in the mutation.  For example `aa:orf1ab:T1001I`

#### To run the program:

```
python3 type_variants.py --fasta-in query.fasta --variants-config config.csv --reference MN908947.fa --variants-out out.csv
```

Which will produce the file `out.csv`:

```
❯ head out.csv
query,ref_count,alt_count,other_count,fraction_alt
seq1,0,9,0,1.0
seq2,0,7,2,0.7778
seq3,0,9,0,1.0
seq4,0,9,0,1.0
seq5,0,9,0,1.0
seq6,0,9,0,1.0
seq7,0,9,0,1.0
seq8,0,9,0,1.0
seq9,0,9,0,1.0
...

```

`ref_count` is the total number of reference alleles in this query

`alt_count` is the total number of predefined alternative alleles in this query

`other_count` is the total number of alleles in this query that are neither `ref` nor `alt` - this includes other valid alleles as well as missing data.

`fraction_alt = alt_count / (ref_count + alt_count + other_count)`

#### Appending genotypes to the output

You can also append the genotype of each variant in the config file to the output with the `--append-genotypes` flag:

```
python3 type_variants.py --fasta-in query.fasta --variants-config config.csv --reference MN908947.fa --variants-out out.withgenotypes.csv --append-genotypes
```

```
❯ head out.withgenotypes.csv
query,ref_count,alt_count,other_count,fraction_alt,snp:C3267T,del:11288:9,aa:orf1ab:T1001I,del:21765:6,snp:G24914C,aa:S:N501Y,snp:A28111G,aa:Orf8:Q27*,aa:N:S235F
seq1,0,9,0,1.0,T,del,I,del,C,Y,G,*,F
seq2,0,7,2,0.7778,T,X,I,X,C,Y,G,*,F
seq3,0,9,0,1.0,T,del,I,del,C,Y,G,*,F
seq4,0,9,0,1.0,T,del,I,del,C,Y,G,*,F
seq5,0,9,0,1.0,T,del,I,del,C,Y,G,*,F
seq6,0,9,0,1.0,T,del,I,del,C,Y,G,*,F
seq7,0,9,0,1.0,T,del,I,del,C,Y,G,*,F
seq8,0,9,0,1.0,T,del,I,del,C,Y,G,*,F
seq9,0,9,0,1.0,T,del,I,del,C,Y,G,*,F
...
```

Each genotype column contains the allele for that variant in each query sequence.

For amino acids, `X` denotes missing data/an untranslatable codon

For deletions, `X` denotes an allele that is neither the reference allele nor the deletion. Deletion variants are otherwise coded as `ref` (same nucleotide sequence as the reference) or `del`.

### Making an alignment in Wuhan-Hu-1 coordinates

If you have a consensus fasta file(s) containing sequences that haven't been aligned to Wuhan-Hu-1, you can make an alignment to feed to this python script using [minimap2](https://github.com/lh3/minimap2), [gofasta](https://github.com/cov-ert/gofasta) and the reference fasta file:

`minimap2 -a -x asm5 MN908947.fa unaligned.fasta | gofasta sam toMultiAlign --reference MN908947.fa > aligned.fasta`

### Run tests

```
python -m unittest discover
```
