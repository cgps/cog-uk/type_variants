#!/bin/bash

set -eu -o pipefail

cat - | minimap2 -a -x asm5 MN908947.fa - | gofasta sam toMultiAlign --reference MN908947.fa | ./to_json.py