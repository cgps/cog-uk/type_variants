#!/usr/bin/env python3

# Most of this file is by Ben Jackson, parts were amended by Ben Taylor

# Copyright 2020 Ben Jackson
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from Bio import SeqIO

import argparse
import csv
import re
import sys

from os.path import dirname, realpath, join
script_dirname = dirname(realpath(__file__))

from collections import namedtuple
Variant = namedtuple('Variant', ['name', 'fullname', 'type', 'reference', 'variant', 'start', 'end', 'source'])

if sys.version_info[0] < 3:
    raise Exception("Python 3 or a more recent version is required.")

def get_nuc_position_from_aa_description(cds, aa_pos):
    """
    given a CDS (eg. S) and the number of an amino acid in it, get the
    1-based start position of that codon in Wuhan-Hu-1 ref coordinates

    nuc_pos is an integer which is 1-based start pos of codon
    """

    # these coordinates are 1-based
    CDS_dict = {"orf1ab": ((266, 13468), (13468, 21555)),
                "orf1a":   (266, 13468),
                "orf1b":   (13468, 21555),
                "s":       (21563, 25384),
                "orf3a":   (25393, 26220),
                "e":       (26245, 26472),
                "m":       (26523, 27191),
                "orf6":    (27202, 27387),
                "orf7a":   (27394, 27759),
                "orf8":    (27894, 28259),
                "n":       (28274, 29533),
                "orf10" :  (29558, 29674)}

    assert cds.lower() in CDS_dict.keys(), f"cds '{cds}' is unknown"

    if cds.lower() == "orf1ab":
        if aa_pos <= 4401:
            parsed_cds = "orf1a"
        else:
            parsed_cds = "orf1b"
            aa_pos = aa_pos - 4401
    else:
        parsed_cds = cds

    cds_tuple = CDS_dict[parsed_cds.lower()]

    nuc_pos = cds_tuple[0] + ((aa_pos - 1) * 3)

    assert nuc_pos <= cds_tuple[1], f"{aa_pos} is not in range {cds_tuple[0]}-{cds_tuple[1]} for cds {cds}"

    return nuc_pos - 1


def parse_variants_in(csvfilehandle, refseq):
    """
    read in a variants file and parse its contents and
    return something sensible.

    format of mutations csv is:

    snp:T6954C
    del:11288:9
    aa:orf1ab:T1001I

    returns variant_list which is a list of dicts of snps, aas and dels,
    one dict per variant. format of subdict varies by variant type
    """

    variant_list = []

    rows = csv.DictReader(csvfilehandle)
    for row in rows:
        mutation_type, _, mutation_details = row['mutation'].partition(":")

        if mutation_type == "snp":
            ref_base = mutation_details[0]
            ref_start = int(mutation_details[1:-1])
            alt_base = mutation_details[-1]
            
            v = Variant(row['mutation'], row['mutation'], mutation_type, ref_base, alt_base, ref_start - 1, ref_start, row['source'])
            assert v.start < len(refseq), f"Mutation {row['mutation']} is outside range 1-{len(refseq)}"
            assert ref_base == refseq[v.start], f"{refseq[v.start]} != {v}"
            variant_list.append(v)

        elif mutation_type == "aa":
            cds, _, mutation_details = mutation_details.partition(":")

            ref_allele = mutation_details[0]
            aa_pos = int(mutation_details[1:-1])
            alt_allele = mutation_details[-1]

            ref_start = get_nuc_position_from_aa_description(cds, aa_pos)
            
            v = Variant(mutation_details, row['mutation'], mutation_type, ref_allele, alt_allele, ref_start, ref_start+3, row['source'])
            assert v.start < len(refseq), f"Mutation {row['mutation']} starts outside range 1-{len(refseq)}"
            assert v.end < len(refseq), f"Mutation {row['mutation']} ends outside range 1-{len(refseq)}"
            ref_allele_check = refseq[v.start:v.end].translate()
            assert ref_allele == ref_allele_check, f"{ref_allele} != {ref_allele_check} ({v})"
            variant_list.append(v)

        elif mutation_type == "del":
            ref_start, length = map(int, mutation_details.split(":"))
            v = Variant(row['mutation'], row['mutation'], mutation_type, refseq[ref_start-1:ref_start-1+length], "-"*length, ref_start-1, ref_start-1+length, row['source'])
            assert v.start < len(refseq), f"Mutation {row['mutation']} starts outside range 1-{len(refseq)}"
            assert v.end < len(refseq), f"Mutation {row['mutation']} ends outside range 1-{len(refseq)}"
            variant_list.append(v)

        else:
            raise AssertionError(f"Couldn't parse {row['mutation']}")

    return(variant_list)

def format_var(record, var):
    if var.type == 'snp':
        return str(record.seq[var.start:var.end]).upper()
    elif var.type == 'aa':
        return str(record.seq[var.start:var.end].translate()).upper()
    elif var.type == 'del':
        return str(record.seq[var.start:var.end].upper())
    raise ValueError(f"Don't know how to format {var.type} mutations")

def is_sequence_error(record, var):
    return not re.match(r'^[ACGT-]+$', str(record.seq[var.start:var.end]), re.IGNORECASE)

def lookup_variants(query, variants):
    results = {}
    for record in SeqIO.parse(query, "fasta"):
        results[record.id] = []
        for var in variants:
            result = format_var(record, var)
            
            if is_sequence_error(record, var):
                state = "sequence error"
            else:
                state = "ref" if str(result).upper() == str(var.reference).upper() else "var" if str(result).upper() == str(var.variant).upper() else "other"
            
            # if var.type == "del":
            #     result = "ref" if result == var.reference else "del" if result == var.variant else "X"

            results[record.id].append((var, result, state))

    return results

def summarise_result(result):
    ref_count = sum(1 for _, _, state in result if state == "ref")
    alt_count = sum(1 for _, _, state in result if state == "var" or state == "del")
    other_count = len(result) - ref_count - alt_count
    fraction_alt = round(alt_count / (alt_count + ref_count + other_count), 4)
    return dict(ref_count=ref_count, alt_count=alt_count, other_count=other_count, fraction_alt=fraction_alt)

def to_csv(variants, results, out_file, write_all_variants = False):
    header = ["query", "ref_count", "alt_count", "other_count", "fraction_alt"]
    if write_all_variants:
        header += [v.name for v in variants]
    csv_output = csv.DictWriter(out_file, header)
    csv_output.writeheader()

    for name, result in results.items():
        row = summarise_result(result)
        row.update({ var.name: state if var.type == "del" else r for var, r, state in result })
        row.update({ "query": name })
        csv_output.writerow({ k: v for k,v in row.items() if k in header })

def parse_args():
    parser = argparse.ArgumentParser(description="""type an alignment in Wuhan-Hu-1 coordinates for variants defined in a config file""",
                                    formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--variants-config', dest = 'variants_in', help="""config file containing variants, see README for details""", default=join(script_dirname, "mutations.csv"), type=argparse.FileType('r'))
    parser.add_argument('--reference', help='Wuhan-Hu-1 in fasta format (for typing the reference allele at deletions and sanity checking the config file)', default=join(script_dirname, "MN908947.fa"), type=argparse.FileType('r'))
    parser.add_argument('--append-genotypes', dest = 'append_genotypes', action = 'store_true', help='if invoked, write the genotype for each variant in the config file to the output')
    parser.add_argument('--fasta-in', help='alignment to type, in fasta format', type=argparse.FileType('r'), default=sys.stdin)
    parser.add_argument('--variants-out', help='csv file to write', type=argparse.FileType('w'), default=sys.stdout)

    args = parser.parse_args()

    return args


if __name__ == '__main__':
    args = parse_args()
    refseq = list(SeqIO.parse(args.reference, "fasta"))[0].seq
    variants = parse_variants_in(args.variants_in, refseq)
    
    results = lookup_variants(args.fasta_in, variants)
    to_csv(variants, results, args.variants_out, args.append_genotypes)
