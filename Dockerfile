FROM ubuntu:20.04 as build

ARG MINIMAP2_VERSION=2.17
ARG GOFASTA_VERSION=v0.0.2

RUN apt update -qy && \
    apt install -qy wget tar && \
    wget --no-verbose https://github.com/lh3/minimap2/releases/download/v${MINIMAP2_VERSION}/minimap2-${MINIMAP2_VERSION}_x64-linux.tar.bz2 && \
    tar -xf minimap2-${MINIMAP2_VERSION}_x64-linux.tar.bz2 && \
    mv minimap2-${MINIMAP2_VERSION}_x64-linux/minimap2 /usr/local/bin/ && \
    chmod +x /usr/local/bin/minimap2 && \
    wget --no-verbose https://github.com/cov-ert/gofasta/releases/download/${GOFASTA_VERSION}/gofasta-linux-amd64 && \
    mv gofasta-linux-amd64 /usr/local/bin/gofasta && \
    chmod +x /usr/local/bin/gofasta

FROM python:3-slim

WORKDIR /work
RUN apt update -qy && apt install -qy build-essential && pip install biopython
COPY --from=build /usr/local/bin/minimap2 /usr/local/bin/gofasta /usr/local/bin/
COPY entrypoint.sh to_json.py MN908947.fa mutations.csv type_variants.py ./
RUN chmod +x *.py *.sh && chmod -R a+wr ./

USER nobody

CMD [ "./entrypoint.sh" ]
