#!/usr/bin/env python3

import json
import sys

from Bio import SeqIO
from os.path import dirname, realpath, join

from type_variants import parse_variants_in, lookup_variants, summarise_result

def format_result(var, result, state):
  variant_type = { "aa": "Amino Acid", "del": "Deletion", "snp": "SNP" }[var.type]
  return dict(name=var.name, type=variant_type, found=str(result).upper(), state=state)

def to_json(result, out_file):
  row = summarise_result(result)
  row['variants'] = [format_result(var, r, state) for var, r, state in result]
  json.dump(row, out_file)

if __name__ == '__main__':
  script_dirname = dirname(realpath(__file__))
  with open(join(script_dirname, "mutations.csv"), "r") as variants_in:
    refseq = list(SeqIO.parse(join(script_dirname, "MN908947.fa"), "fasta"))[0].seq
    variants = parse_variants_in(variants_in, refseq)
  results = lookup_variants(sys.stdin, variants)
  result, = results.values()
  to_json(result, sys.stdout)
